from collections import Iterable

class GmshMeshObject(object):
    
    """ Constructor """
    def __init__(self,fname):
        self.filename=fname
        self.nodes=[]
        self.elements=[]
        self.types={
            1:'2-node line',
            2:'3-node triangle',
            3:'4-node quadrangle',
            4:'4-node tetrahedron',
            5:'8-node hexahedron',
            6:'6-node prism',
            7:'5-node pyramid',
            8:'3-node second order line (2 nodes associated with the vertices and 1 with the edge)',
            9:'6-node second order triangle (3 nodes associated with the vertices and 3 with the edges)',
            10:'9-node second order quadrangle (4 nodes associated with the vertices, 4 with the edges and 1 with the face)',
            11:'10-node second order tetrahedron (4 nodes associated with the vertices and 6 with the edges)',
            12:'27-node second order hexahedron (8 nodes associated with the vertices, 12 with the edges, 6 with the faces and 1 with the volume)',
            13:'18-node second order prism (6 nodes associated with the vertices, 9 with the edges and 3 with the quadrangular faces)',
            14:'14-node second order pyramid (5 nodes associated with the vertices, 8 with the edges and 1 with the quadrangular face)',
            15:'1-node point',
            16:'8-node second order quadrangle (4 nodes associated with the vertices and 4 with the edges)',
            17:'20-node second order hexahedron (8 nodes associated with the vertices and 12 with the edges)',
            18:'15-node second order prism (6 nodes associated with the vertices and 9 with the edges)',
            19:'13-node second order pyramid (5 nodes associated with the vertices and 8 with the edges)',
            20:'9-node third order incomplete triangle (3 nodes associated with the vertices, 6 with the edges)',
            21:'10-node third order triangle (3 nodes associated with the vertices, 6 with the edges, 1 with the face)',
            22:'12-node fourth order incomplete triangle (3 nodes associated with the vertices, 9 with the edges)',
            23:'15-node fourth order triangle (3 nodes associated with the vertices, 9 with the edges, 3 with the face)',
            24:'15-node fifth order incomplete triangle (3 nodes associated with the vertices, 12 with the edges)',
            25:'21-node fifth order complete triangle (3 nodes associated with the vertices, 12 with the edges, 6 with the face)',
            26:'4-node third order edge (2 nodes associated with the vertices, 2 internal to the edge)',
            27:'5-node fourth order edge (2 nodes associated with the vertices, 3 internal to the edge)',
            28:'6-node fifth order edge (2 nodes associated with the vertices, 4 internal to the edge)',
            29:'20-node third order tetrahedron (4 nodes associated with the vertices, 12 with the edges, 4 with the faces)',
            30:'35-node fourth order tetrahedron (4 nodes associated with the vertices, 18 with the edges, 12 with the faces, 1 in the volume)',
            31:'56-node fifth order tetrahedron (4 nodes associated with the vertices, 24 with the edges, 24 with the faces, 4 in the volume)',
            92:'64-node third order hexahedron (8 nodes associated with the vertices, 24 with the edges, 24 with the faces, 8 in the volume)',
            93:'125-node fourth order hexahedron (8 nodes associated with the vertices, 36 with the edges, 54 with the faces, 27 in the volume)',
        }
        
        # read all lines from the file and close the file
        fid = open(fname,'r')
        self.lines=fid.readlines()
        lines=self.lines
        fid.close()
        
        # find the line indexes for the node block
        nodelineid=0
        while lines[nodelineid]!='$Nodes\n':
            nodelineid = nodelineid+1
            
        # find the line indexes for the element block
        elelineid=0
        while lines[elelineid]!='$Elements\n':
            elelineid = elelineid+1

        # read the node coordinates
        nnodes=map(int,lines[nodelineid+1].split())[0]
        for line in lines[nodelineid+2:nodelineid+nnodes+2]:
            self.nodes.append(map(float,line.split()[1:]))
            
            
        # read the elements
        neles=map(int,lines[elelineid+1].split())[0]
        for line in lines[elelineid+2:elelineid+neles+2]:
            self.elements.append(map(int,line.split()[1:]))
            
    """ METHODS """

    # Returns the number of tags for elements listed in 'eles'
    def numof_element_tags(self,eles):
        if isinstance(eles, Iterable)==True:
            return [self.elements[i][1] for i in eles]
        else:
            return self.elements[eles][1]

    # Returns the tags of the elements listed in 'eles' 
    def element_tags(self,eles):
        if isinstance(eles, Iterable)==True:
            return [self.elements[i][2:2+self.numof_element_tags(i)] for i in eles]
        else:
            return self.elements[eles][2:2+self.numof_element_tags(eles)]

    # Returns the nodes of elements listed in 'eles'
    def element_to_nodes(self,eles):
        if isinstance(eles,Iterable)==True:
            return [self.elements[i][2+self.numof_element_tags(i):] for i in eles]
        else:
            return self.elements[eles][2+self.numof_element_tags(eles):]

    # Returns the total number of nodes
    def numof_nodes(self):
        return len(self.nodes)
    
    # Returns the number of elements
    def numof_elements(self):
        return len(self.elements)

    # Returns the integer values corresponding to the element types for the elements listed in 'eles'
    def element_type_int(self,eles):
        if isinstance(eles,Iterable)==True:
            return [self.elements[i][0] for i in eles]
        else:
            return self.elements[eles][0]
    
    # Returns the string values corresponding to the element types for the element listed in 'eles'
    def element_type_string(self,eles):
        if isinstance(eles,Iterable)==True:
            return [self.types[i] for i in self.element_type_int(eles)]
        else:
            return self.types[self.element_type_int(eles)]


