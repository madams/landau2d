# Class for handling the axisymmetric 
# Landau collision operator

#from numba import jit
from collections import Iterable
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import matplotlib.pyplot as plt
from matplotlib import interactive
import numpy as np
import scipy.special as sp
from GmshMeshObject import GmshMeshObject
from Tri3 import Tri3
from Tri6 import Tri6

class Landau(object):

    # Read the gmsh data:
    # Store nodes, triangles, connectivity,
    # and Dirichlet boundary
    def __init__(self,fname):

        # tolerance for comparing whether a float is 'zero'
        self.zerotol=1.e-10

        # Read the mesh
        # this could use some refinement, e.g., 
        # check that the input is 'OK'
        msh=GmshMeshObject(fname)
   
        # list of global nodes in the mesh.
        # cut the extra third dimension as unnecessary
        self.nodes=np.array(msh.nodes)[:,:2] 
        
        # list of triangles
        self.tris=[] 

        # a map from local triangle nodes to global nodes.
        self.triToNodes=[]

        # list to indicate if a node belongs to the Dirichlet boundary or not
        self.isDirichletBoundary=[0 for i in range(len(self.nodes))]

        # Create triangles from the nodes and find the Dirichlet boundary
        for i in range(len(msh.elements)):
            if msh.element_type_int(i)==2: # '3-node triangle' in \Omega
                inodes = msh.element_to_nodes(i)
                self.tris.append(Tri3([self.nodes[inode-1] for inode in inodes]))
                self.triToNodes.append(inodes)
            elif msh.element_type_int(i)==9: # '6-node second order triangle' in \Omega
                inodes = msh.element_to_nodes(i)
                self.tris.append(Tri6([self.nodes[inode-1] for inode in inodes]))
                self.triToNodes.append(inodes)
            else: # at \partial\Omega
                if msh.element_tags(i)[0]==1: # at Dirichlet boundary
                    inodes = msh.element_to_nodes(i)
                    for inode in inodes:
                        self.isDirichletBoundary[inode-1]=1
        
        # The number of dofs per element
        self.nshapes=len(self.triToNodes[0])



    # projects a function f(r,z) to the mesh
    # the boundary condition can be given 
    # in a separate function g(r,z)
    # Returns the degrees of freedom
    # so that f can be evaluated according to
    # 
    # f=\sum_i f_i * \lambda_i(r,z)
    #@jit
    def project(self,f,g):

        # the FEM projection matrix
        proj_mat=np.zeros((len(self.nodes),len(self.nodes)))

        # the FEM projection vector
        proj_vec=np.zeros(len(self.nodes))
        
        # local FEM projection matrix
        proj_mat_local=np.zeros((self.nshapes,self.nshapes))

        # local FEM projection vector 
        proj_vec_local=np.zeros(self.nshapes) 
        
        # Assemble the projection matrix and vector
        for k in range(len(self.tris)): 
            
            # Quadrature points and weights for the tetra 'k'
            qx=self.tris[k].quad_points()
            qw=self.tris[k].quad_weights()

            # Function f values at the quadrature points
            fx=np.array(f(qx[:,0],qx[:,1]))
            
            # shape functions for the projection
            shapes=self.tris[k].eval_shape_values(qx).transpose()
            
            # Compute the local system vector and matrix
            for i in range(self.nshapes):
                proj_vec_local[i]=np.dot(shapes[i]*fx*qx[:,0],qw)
                for j in range(self.nshapes):
                    proj_mat_local[i][j]=np.dot(shapes[i]*shapes[j]*qx[:,0],qw)
                    
            # Map the local coordinates to global ones.
            # Skip the boundary nodes since these are 
            # set separately according to the Dirichlet condition
            for i in range(self.nshapes):
                if self.isDirichletBoundary[self.triToNodes[k][i]-1]==0:
                    proj_vec[self.triToNodes[k][i]-1]+=proj_vec_local[i]
                    for j in range(self.nshapes):
                        proj_mat[self.triToNodes[k][i]-1,self.triToNodes[k][j]-1]+=proj_mat_local[i][j],
                                
        # Assemble the Dirichlet boundary condition
        for i in range(len(self.nodes)):
            if self.isDirichletBoundary[i]==1:
                proj_vec[i]=g(self.nodes[i,0],self.nodes[i,1])
                proj_mat[i][i]=1
                                            
        # Solve the system and return the values.
        return np.linalg.solve(proj_mat,proj_vec)


    # projects a function f(r,z) to the mesh
    # the boundary condition can be given 
    # in a separate function g(r,z)
    # Returns the degrees of freedom
    # so that f can be evaluated according to
    # 
    # f=\sum_i f_i * \lambda_i(r,z)
    def projectFast(self,f,g):

        # the FEM projection vector
        fs=np.zeros(len(self.nodes))
        
        # Loop through the nodes
        for i in range(len(self.nodes)):
            if self.isDirichletBoundary[i]==1:
                fs[i]=g(self.nodes[i,0],self.nodes[i,1])
            else:
                fs[i]=f(self.nodes[i,0],self.nodes[i,1])

        return fs
    

    # This function returns the unmodified mass or projection matrix
    #@jit
    def massMatrix(self):

        # the FEM projection matrix
        mat=np.zeros((len(self.nodes),len(self.nodes)))

        # local FEM projection matrix
        mat_local=np.zeros((self.nshapes,self.nshapes))
        
        # Assemble the projection matrix
        for k in range(len(self.tris)): 
            
            # Quadrature points and weights for the tetra 'k'
            qx=self.tris[k].quad_points()
            qw=self.tris[k].quad_weights()

            # shape functions for the projection
            shapes=self.tris[k].eval_shape_values(qx).transpose()
            
            # Compute the local system vector and matrix
            for i in range(self.nshapes):
                for j in range(self.nshapes):
                    mat_local[i][j]=np.dot(shapes[i]*shapes[j]*qx[:,0],qw)
                    
            # Map the local coordinates to global ones.
            for i in range(self.nshapes):
                for j in range(self.nshapes):
                    mat[self.triToNodes[k][i]-1,self.triToNodes[k][j]-1]+=mat_local[i][j],
                                
        return mat


    
    # given the degrees-of-freedom, fs, from projecting function f,
    # assembles the collision matrix
    #@jit
    def collisionMatrix(self,fs):

        # the global collision matrix
        c_global=np.zeros((len(self.nodes),len(self.nodes)))

        # assemble triangle at a time
        for k in range(len(self.tris)): 

            # Print the status of the process
            if np.mod(k,int((len(self.tris))/10))==0:
                print '    processing element ',k,'/',len(self.tris)
            
            # Quadrature points and weights for the triangle 'k'
            qx=self.tris[k].quad_points()
            qw=self.tris[k].quad_weights()

            # initialize the local collision matrix
            c_local=np.zeros((self.nshapes,self.nshapes))
            
            # loop through the quad points to
            # accumulate the local collision matrix
            for ix in range(len(qx)):
                
                # evaluate the r*K and r*D
                [rK,rD]=self.rKD(qx[ix,0],qx[ix,1],fs)

                # the values and gradients of the shape functions at qx
                grads=self.tris[k].eval_shape_grads(qx[ix,:]) # gradients are constant for linear shapes
                shapes=self.tris[k].eval_shape_values(qx[ix,:])

                # contribution to the local matrix
                for i in range(self.nshapes):
                    for j in range(self.nshapes):
                        c_local[i,j]+=np.dot(grads[i],rK*shapes[j]-np.dot(rD,grads[j]))*qw[ix]
                

            # Map the local contribution to the global matrix
            # Skip the boundary nodes since these are 
            # set separately according to the Dirichlet condition
            for i in range(self.nshapes):
                #if self.isDirichletBoundary[self.triToNodes[k][i]-1]==0:
                for j in range(self.nshapes):
                    c_global[self.triToNodes[k][i]-1,self.triToNodes[k][j]-1]+=c_local[i][j]            

        # return the global matrix
        return c_global


    # given degrees-of-freedom, fs, for function f,
    # computes the friction vector and diffusion tensor
    # scaled by r (the jacobian) at a requested point (r,z)
    #
    # the result will be a vector [ [r*Kr, r*Kz], [[r*Drr, r*Drz],[r*Dzr, r*Dzz]] ]
    #@jit
    def rKD(self,r,z,fs):

        # initialize the values that are to be returned
        rK=np.zeros(2)
        rD=np.zeros((2,2))
    
        # compute the integrals triangle by triangle
        for k in range(len(self.tris)): 

            # Quadrature points and weights for the triangle 'k'
            qx=self.tris[k].quad_points()
            qw=self.tris[k].quad_weights()

            # temporary storages
            rKk=np.zeros(2)
            rDk=np.zeros((2,2))

            # loop through the quad points
            for ix in range(len(qx)):

                # the tensors for computing K and D
                #print '-------------------'
                #print 'Computing Ud and Uk'
                [Ud,Uk]=self.Uab(r,z,qx[ix,0],qx[ix,1])

                # the values and gradients of the shape functions at y
                grads=self.tris[k].eval_shape_grads(qx[ix,:]) # gradients are constant for linear shapes
                shapes=self.tris[k].eval_shape_values(qx[ix,:])
            
                # compute contributions from this quadrature point to Kk and Dk
                rKk+=np.dot(np.dot(Uk,grads.transpose()),fs[[y-1 for y in self.triToNodes[k]]])*qw[ix]*qx[ix,0]*r
                rDk+=Ud*np.dot(shapes,fs[[y-1 for y in self.triToNodes[k]]])*qw[ix]*qx[ix,0]*r
            
            # add the element wise contribution to the global value
            rK+=rKk
            rD+=rDk

        # return the global values
        return [rK, rD]


    # returns the axially averaged landau tensors U^{\alpha\beta} and U^{\alpha\bar{\beta}}
    def Uab(self,r,z,rp,zp):
        if abs(r-rp)<self.zerotol and abs(z-zp)<self.zerotol:
            return np.array([np.zeros((2,2)), np.zeros((2,2))])
        else:
            l=r**2+rp**2+(z-zp)**2
            s=2.*r*rp/l
            i1func=self.i1(s)
            i2func=self.i2(s)
            i3func=self.i3(s)
            U_r_r=4.*np.pi*np.power(l,-1.5)*(rp**2*i1func+(z-zp)**2*i2func)
            U_r_z=4.*np.pi*np.power(l,-1.5)*(zp-z)*(r*i2func-rp*i3func)
            U_z_r=U_r_z
            U_z_z=4.*np.pi*np.power(l,-1.5)*((r**2+rp**2)*i2func-2.*r*rp*i3func)
            U_r_rp=4.*np.pi*np.power(l,-1.5)*((z-zp)**2*i3func+r*rp*i1func)
            U_r_zp=U_r_z
            U_z_rp=4.*np.pi*np.power(l,-1.5)*(zp-z)*(r*i3func-rp*i2func)
            U_z_zp=U_z_z
            return np.array([ [[U_r_r,U_r_z],[U_z_r,U_z_z]], [[U_r_rp,U_r_zp],[U_z_rp,U_z_zp]] ])


    # the special functions required for evaluating the axially averaged Landau tensor
    def i1(self,x):
        if abs(x)<=self.zerotol:
            return np.pi/2.
        else:
            return 4./(x**2*np.sqrt(1.+x))*(sp.ellipk(2.*x/(1.+x))-(1.+x)*sp.ellipe(2.*x/(1.+x)))
    
    def i2(self,x):
        if abs(x)<=self.zerotol:
            return np.pi
        else:
            return 2./((1.-x)*np.sqrt(1.+x))*sp.ellipe(2.*x/(1.+x))

    def i3(self,x):
        if abs(x)<=self.zerotol:
            return 2.*np.pi
        else:
            return 2./((1.-x)*x*np.sqrt(1.+x))*(sp.ellipe(2.*x/(1.+x))-(1.-x)*sp.ellipk(2.*x/(1.+x)))


    # Given the dofs of the solution
    # this function plots the solution over the triangles
    def plot(self,fs,fig_id):
        # get the dofs on the triangle k

        fig = plt.figure(fig_id,figsize=(10,6))
        plt.clf()
        ax = fig.add_subplot(111, projection='3d')
        for k in range(len(self.tris)):
            # The dofs on the triangle            
            phis=fs[[y-1 for y in self.triToNodes[k]]]
            # plot the triangle
            self.tris[k].plot(phis,ax)
            
            
        ax.set_xlabel('r')
        ax.set_ylabel('z')
        ax.set_aspect('auto')
        plt.show()
