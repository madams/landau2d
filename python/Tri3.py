import numpy as np
from collections import Iterable

# Class for handling a three point triangular element
class Tri3(object):
    
    
    # Constructor takes in the tree vertices of the triangle in 
    # a numpy array:
    #
    # xs=numpy.array([[x0,y0],[x1,y1],[x2,y2]])
    #
    # On a unit triangle, the points are ordered according to
    #
    # v                                                              
    # ^                  
    # |                  
    # 2
    # |`\                     
    # |  `\             
    # |    `\           
    # |      `\         
    # |        `\       
    # 0----------1 --> u
    #
    def __init__(self,xs):

        self.xs=np.array(xs)
        self.Bvec=np.array(self.xs[0])
        self.Amat=np.array([self.xs[1]-self.xs[0],
                            self.xs[2]-self.xs[0]]).transpose()
        self.Ainv=np.linalg.inv(self.Amat)
        self.Jac=np.linalg.det(self.Amat)
        self.Area=self.Jac/2
        
        # order-3 gauss quadrature points on unit triangle
        # source http://math2.uncc.edu/~shaodeng/TEACHING/math5172/Lectures/Lect_15.PDF
        self.qs=np.array([[0.3333333333333333, 0.3333333333333333],
                          [0.2000000000000000, 0.6000000000000000],
                          [0.2000000000000000, 0.2000000000000000],
                          [0.6000000000000000, 0.2000000000000000]])
        # weights for the quadrature points
        self.qw=np.array([-0.5625000000000000,
                           0.5208333333333333,
                           0.5208333333333333,
                           0.5208333333333333])



    # Returns quadrature points
    def quad_points(self):
        return self.unit_to_element(self.qs)

    # Returns quadrature weights
    def quad_weights(self):
        return self.qw*self.Area
        
        
    # Evaluates the shape functions  
    #
    # input:
    # x=[[x0,y0],[x1,y1],...]
    #
    # output:
    # result = [[s0(x0,y0),s1(x0,y0),...],[s0(x1,y1),s1(x1,y1),...],...]
    #
    def eval_shape_values(self,x):
        tmp=np.array(x)
        u=self.element_to_unit(tmp)
        if tmp.size>3:
            return np.array([[1-y[0]-y[1],y[0],y[1]] for y in u])
        else:
            return np.array([1-u[0]-u[1],u[0],u[1]])
    

    # Evaluates the gradients of the shape functions
    #
    # input:
    # x=[[x0,y0],[x1,y1],...]
    #
    # output:
    # result = [[[grad(s0)(x0,y0)],[grad(s1)(x0,y0)],...],
    #           [[grad(s0)(x1,y1)],[grad(s1)(x1,y1)],...],
    #           ...                                            ] 
    #
    def eval_shape_grads(self,x):
        tmp=np.array(x)
        grad=np.dot([[-1,-1],[1,0],[0,1]],self.Ainv)
        if tmp.size>2:
            return np.array([grad for y in tmp])
        else:
            return grad
    
    # Transforms coordinates from a unit tetra to the element
    # 
    # input:
    # u=numpy.array([[u0,v0],[u1,v1],...])
    #
    # output:
    # x=numpy.array([[x0,y0],[x1,y1],...])
    #
    def unit_to_element(self,u):
        tmp=np.array(u)
        if tmp.size>2:
            return np.array([self.Bvec+np.dot(self.Amat,y) for y in tmp])
        else:
            return np.array(self.Bvec+np.dot(self.Amat,tmp))

    # Transforms coordinates from the element to a unit tetra
    # 
    # input:
    # x=numpy.array([[x0,y0],[x1,y1],...])
    #
    # output:
    # u=numpy.array([[u0,v0],[u1,v1],...])
    #
    def element_to_unit(self,x):
        tmp=np.array(x)
        if tmp.size>2:
            return np.array([np.dot(self.Ainv,y-self.Bvec) for y in tmp])
        else:
            return np.array(np.dot(self.Ainv,tmp-self.Bvec))


    # Given the nodal values, this function plots 
    # the function over the triangle on the given 
    # axis 'ax'
    def plot(self,fs,ax):
        us=np.array([[0., 0.],[1., 0.],[0., 1.]])
        xs=self.unit_to_element(us)
        clr=np.random.rand(4)
        ax.plot_trisurf(xs[:,0],xs[:,1], fs, linewidth=0.2, alpha=1., color='red')#np.random.rand(3))

    # Given the nodal values,
    # this function plots the gradient
    # over the triangle
    def plot_grad(self,fs,fig_id):
        # get the dofs on the triangle k
        
        fig = plt.figure(fig_id)
        plt.clf()
        ax1 = fig.add_subplot(211, projection='3d')
        ax2 = fig.add_subplot(212, projection='3d')
        
        us=np.array([[0., 0.],[1., 0.],[0., 1.]])
        xs=self.unit_to_element(us)
        grads_at_xs=self.eval_shape_grads(xs)
        grad_at_xs=np.array([np.dot(fs,grads) for grads in grads_at_xs])
        ax1.plot_trisurf(xs[:,0],xs[:,1], grad_at_xs[:,0], linewidth=0.2, alpha=1., color = np.random.rand(3))
        ax2.plot_trisurf(xs[:,0],xs[:,1], grad_at_xs[:,1], linewidth=0.2, alpha=1., color = np.random.rand(3))
            
            
        ax1.set_xlabel('r')
        ax1.set_ylabel('z')
        ax2.set_xlabel('r')
        ax2.set_ylabel('z')
        #plt.colorbar(sc)
        plt.show()

