from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import matplotlib.pyplot as plt
from matplotlib import interactive

from Landau_mpi import Landau
import numpy as np

# For holding the plots after script finishes
#interactive(True)

# global constants
sigmasq=0.05
tol=1.e-10

# Define the function that is to be projected
def f(r,z):
    return 0.5*np.power(np.pi*sigmasq,-1.5)*np.exp(-(r**2+(z-0.5)**2)/sigmasq)+0.5*np.power(np.pi*sigmasq,-1.5)*np.exp(-(r**2+z**2)/sigmasq)

def zero(r,z):
    return 0.

def zfunc(r,z):
    return z

def energy(r,z):
    return r**2+z**2


# initialize the Landau stuff
#landau=Landau('half_circle.msh')
landau=Landau('rzplate.msh')

# project the function f into the mesh
fs=landau.project(f,zero)

if(landau.para.rank==0):
    # project the function f(r,z)=z into the mesh
    # for testing out the conservation property
    zs=landau.project(zfunc,zfunc)
    es=landau.project(energy,energy)

# compile the collision matrix and evaluate the moments
c_mat=landau.collisionMatrix(fs)

if(landau.para.rank==0):
    print 'z-moment: ',np.dot(zs,np.dot(c_mat,fs))
    print 'r^2+z^2 moment: ',np.dot(es,np.dot(c_mat,fs))
        
    # plot the projection.
    #landau.plot(fs,1)

# # Plot the z
# fig = plt.figure(1)
# plt.clf()
# ax = fig.add_subplot(211, projection='3d')
# sc=ax.plot_trisurf(landau.nodes[:,0],landau.nodes[:,1], fs, cmap=cm.hot, linewidth=0.2)
# ax.set_xlim([0,2])
# ax.set_ylim([-2,2])
# ax.set_title('FEM Maxwellian')
# ax.set_xlabel('r')
# ax.set_ylabel('z')
# plt.colorbar(sc)
# ax2 = fig.add_subplot(212, projection='3d')
# sc2=ax2.plot_trisurf(landau.nodes[:,0], landau.nodes[:,1], zs, cmap=cm.hot, linewidth=0.2)
# ax2.set_title('f(r,z) = z')
# ax2.set_xlabel('r')
# ax2.set_ylabel('z')
# ax2.set_xlim([0,2])
# ax2.set_ylim([-2,2])
# plt.colorbar(sc2)
# plt.show()






