from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import matplotlib.pyplot as plt
from matplotlib import interactive

from Landau import Landau
import numpy as np

# For holding the plots after script finishes
interactive(True)

# global constants
sigmasq=0.05
tol=1.e-10

# time stepping parameters
dt=1.e-3
nt=1
iterations_per_timestep=6

# Define the function that is to be projected
def f(r,z):
    return 0.5*np.power(np.pi*sigmasq,-1.5)*np.exp(-(r**2+(z-0.5)**2)/sigmasq)+0.5*np.power(np.pi*sigmasq,-1.5)*np.exp(-(r**2+z**2)/sigmasq)

def zero(r,z):
    return 0.

def zfunc(r,z):
    return z

def energy(r,z):
    return r**2+z**2


# initialize the Landau stuff
landau=Landau('rzplate.msh')

# project the function f into the mesh
f0=landau.projectFast(f,zero)
#f0fast=landau.projectFast(f,zero)

# project the functions f(r,z)=z and f(r,z)=r^2+z^2
# onto the mesh for testing out the conservation properties
zs=landau.projectFast(zfunc,zfunc)
#zsslow=landau.project(zfunc,zfunc)
es=landau.projectFast(energy,energy)

# the raw mass matrix (without boundary conditions)
m_mat=landau.massMatrix()

# initialize variables for storing the solution vector
fs=np.zeros((len(f0),nt+1))
fprev=np.zeros(len(f0))
fnext=np.zeros(len(f0))

# set the initial state
fs[:,0]=f0

# time stepping loop using implicit euler
for i in range(nt):
    
    print '***********************************'
    print 'Time step ',i+1, '/',nt
    print 'energy: ',np.dot(es,np.dot(m_mat,fs[:,i]))
    print 'momentum: ',np.dot(zs,np.dot(m_mat,fs[:,i]))
    
    # set the first iterate
    fprev=fs[:,i]

    # iterate one time step
    for j in range(iterations_per_timestep):
        
        print 'iteration: ',j+1,'/',iterations_per_timestep
               
        # picard step for the implicit euler discretization
        c_mat=landau.collisionMatrix(fprev)
        fnext=np.linalg.solve(m_mat-dt*c_mat,np.dot(m_mat,fs[:,i]))

        print 'dummy z conservation of the operator: ',np.dot(zs,np.dot(c_mat,fprev))
        print 'dummy energy conservation of the operator: ',np.dot(es,np.dot(c_mat,fprev))
        
        print 'z-moment time derivative: ',np.dot(zs,np.dot(c_mat,fnext))
        print 'r^2+z^2-moment time derivative: ',np.dot(es,np.dot(c_mat,fnext))
        fprev=fnext
    
    # store the result from the iteration 
    fs[:,i+1]=fnext

    # print the difference in momentum and energy compared to the initial state
    print 'z total difference: ', np.dot(zs,np.dot(m_mat,fs[:,i+1]-f0))
    print 'r^2+z^2 total difference: ',np.dot(es,np.dot(m_mat,fs[:,i+1]-f0))


# plot the solution (this is not the best way)
for i in range(nt+1):
    landau.plot(fs[:,i],i+1)











