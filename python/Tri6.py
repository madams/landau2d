import numpy as np
from collections import Iterable

from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import matplotlib.pyplot as plt
from matplotlib import interactive

# Class for handling a six point tetrahedral element
class Tri6(object):
    
    
    # Constructor takes in the six points of the triangle in 
    # a numpy array:
    #
    # xs=numpy.array([[x0,y0],[x1,y1],[x2,y2],[x3,y3],[x4,y4],[x5,y5]])
    #
    # On a unit triangle, the points are referred to as
    #
    # v                                                              
    # ^                  
    # |                  
    # 2                  
    # |`\                
    # |  `\              
    # 5    `4            
    # |      `\          
    # |        `\        
    # 0-----3----1 --> u 
    # 
    def __init__(self,xs):

        self.xs=np.array(xs)
        self.Bvec=np.array(self.xs[0])
        self.Amat=np.array([self.xs[1]-self.xs[0],
                            self.xs[2]-self.xs[0]]).transpose()
        self.Ainv=np.linalg.inv(self.Amat)
        self.Jac=np.linalg.det(self.Amat)
        self.Area=self.Jac/2.
        
        # order-5 gauss quadrature points on unit triangle
        # source http://math2.uncc.edu/~shaodeng/TEACHING/math5172/Lectures/Lect_15.PDF
        self.qs=np.array([[0.33333333333333, 0.33333333333333],
                          [0.47014206410511, 0.47014206410511],
                          [0.47014206410511, 0.05971587178977],
                          [0.05971587178977, 0.47014206410511],
                          [0.10128650732346, 0.10128650732346],
                          [0.10128650732346, 0.79742698535309],
                          [0.79742698535309, 0.10128650732346]])

        # weights for the quadrature points
        self.qw=np.array([0.22500000000000,
                          0.13239415278851,
                          0.13239415278851,
                          0.13239415278851,
                          0.12593918054483,
                          0.12593918054483,
                          0.12593918054483])


    # Returns quadrature points
    def quad_points(self):
        return self.unit_to_element(self.qs)

    # Returns quadrature weights
    def quad_weights(self):
        return self.qw*self.Area
        
        
    # Evaluates the shape functions  
    #
    # input:
    # x=[[x0,y0],[x1,y1],...]
    #
    # output:
    # result = [[s0(x0,y0),s1(x0,y0),...],[s0(x1,y1),s1(x1,y1),...],...]
    #
    def eval_shape_values(self,x):
        tmp=np.array(x)
        u=self.element_to_unit(tmp)
        if tmp.size>2:
            return np.array([[(1.-y[0]-y[1])*(1.-2.*y[0]-2.*y[1]),y[0]*(2.*y[0]-1.),y[1]*(2.*y[1]-1.),4.*y[0]*(1.-y[0]-y[1]),4.*y[0]*y[1],4.*y[1]*(1.-y[0]-y[1])] for y in u])
        else:
            return np.array([(1.-u[0]-u[1])*(1.-2.*u[0]-2.*u[1]),u[0]*(2.*u[0]-1.),u[1]*(2.*u[1]-1.),4.*u[0]*(1.-u[0]-u[1]),4.*u[0]*u[1],4.*u[1]*(1.-u[0]-u[1])])
    

    # Evaluates the gradients of the shape functions
    #
    # input:
    # x=[[x0,y0],[x1,y1],...]
    #
    # output:
    # result = [[[grad(s0)(x0,y0)],[grad(s1)(x0,y0)],...],
    #           [[grad(s0)(x1,y1)],[grad(s1)(x1,y1)],...],
    #           ...                                            ] 
    #
    def eval_shape_grads(self,x):
        tmp=np.array(x)
        u=self.element_to_unit(tmp)
        if tmp.size>2:
            return np.array([np.dot([ [-3.+4.*y[0]+4.*y[1],-3.+4.*y[0]+4.*y[1]] , [4.*y[0]-1.,0.] , [0.,4.*y[1]-1.] , [4.*(1.-2.*y[0]-y[1]),-4.*y[0]] , [4.*y[1],4*y[0]] , [-4.*y[1],4.*(1.-y[0]-2.*y[1])] ],self.Ainv) for y in u])
        else:
            return np.array(np.dot([ [-3.+4.*u[0]+4.*u[1],-3.+4.*u[0]+4.*u[1]] , [4.*u[0]-1.,0.] , [0.,4.*u[1]-1.] , [4.*(1.-2.*u[0]-u[1]),-4.*u[0]] , [4.*u[1],4.*u[0]] , [-4.*u[1],4.*(1.-u[0]-2.*u[1])] ],self.Ainv))
    
    # Transforms coordinates from a unit tetra to the element
    # 
    # input:
    # u=numpy.array([[u0,v0],[u1,v1],...])
    #
    # output:
    # x=numpy.array([[x0,y0],[x1,y1],...])
    #
    def unit_to_element(self,u):
        tmp=np.array(u)
        if tmp.size>2:
            return np.array([self.Bvec+np.dot(self.Amat,y) for y in tmp])
        else:
            return np.array(self.Bvec+np.dot(self.Amat,tmp))

    # Transforms coordinates from the element to a unit tetra
    # 
    # input:
    # x=numpy.array([[x0,y0],[x1,y1],...])
    #
    # output:
    # u=numpy.array([[u0,v0],[u1,v1],...])
    #
    def element_to_unit(self,x):
        tmp=np.array(x)
        if tmp.size>2:
            return np.array([np.dot(self.Ainv,y-self.Bvec) for y in tmp])
        else:
            return np.array(np.dot(self.Ainv,tmp-self.Bvec))


    # Given the nodal values, this function plots 
    # the function over the triangle on given 
    # axis 'ax'
    def plot(self,fs,ax):
        us=np.array([[0., 0.],[1., 0.],[0., 1.],[0.5, 0.],[0.5, 0.5],[0., 0.5]])
        xs=self.unit_to_element(us)
        clr=np.random.rand(4)
        ax.plot_trisurf(xs[:,0],xs[:,1], fs, linewidth=0.2, alpha=1., color='red')


    # Given the nodal values,
    # this function plots the gradient of
    # over the triangle
    def plot_grad(self,fs,fig_id):
        # get the dofs on the triangle k
        
        fig = plt.figure(fig_id)
        plt.clf()
        ax1 = fig.add_subplot(211, projection='3d')
        ax2 = fig.add_subplot(212, projection='3d')
        
        us=np.array([[0., 0.],[1., 0.],[0., 1.],[0.5, 0.],[0.5, 0.5],[0., 0.5]])
        xs=self.unit_to_element(us)
        grads_at_xs=self.eval_shape_grads(xs)
        grad_at_xs=np.array([np.dot(fs,grads) for grads in grads_at_xs])
        ax1.plot_trisurf(xs[:,0],xs[:,1], grad_at_xs[:,0], linewidth=0.2, alpha=1., color = np.random.rand(3))
        ax2.plot_trisurf(xs[:,0],xs[:,1], grad_at_xs[:,1], linewidth=0.2, alpha=1., color = np.random.rand(3))
            
            
        ax1.set_xlabel('r')
        ax1.set_ylabel('z')
        ax2.set_xlabel('r')
        ax2.set_ylabel('z')
        #plt.colorbar(sc)
        plt.show()
