SHELL=/bin/bash
# -*- mode: makefile -*-
all: landaufem

CPPFLAGS = ${LAND_INC}
include ${PETSC_DIR}/lib/petsc/conf/variables
include ${PETSC_DIR}/lib/petsc/conf/rules

# list of objects for the main program
MAIN_OBJS = main.o landau.o

landaufem: ${MAIN_OBJS} chkopts
	-${CLINKER} -o $@.${PETSC_ARCH} ${MAIN_OBJS} ${PETSC_LIB} ${LAND_LIB}
	-${DSYMUTIL} $@
	${RM} -f $(MAIN_OBJS)

NP=1
ORDER=2
TENSOR=0
REFINE=2
MEM=0
NS=2
DT=5.e-4
NOP1=$(shell echo $(ORDER) + 1 | bc)
#KMP_AFFINITY=balanced
OMP_NUM_THREADS=1 
KMP_AFFINITY=compact
KMP_PLACE_THREADS=2T 
VERB=2

VALGRIND_PROG=valgrind
VALGRIND_ARGS=--dsymutil=yes --leak-check=no --gen-suppressions=no --num-callers=20 --error-limit=no
VAL=$(VALGRIND_PROG) $(VALGRIND_ARGS)

run-gmsh:
	-${MPIEXEC} -n ${NP} ./landaufem.${PETSC_ARCH} -dm_refine ${REFINE} -f ../python/rzplatesimplex.msh -snes_rtol 1.e-8 -snes_stol 1.e-8 -ts_type theta -ts_theta_theta .5 -ts_theta_endpoint -pc_type lu -ksp_type preonly -petscspace_order ${ORDER} -mass_petscspace_order ${ORDER} -mass_petscspace_poly_tensor 0 -petscspace_poly_tensor 0 -ts_dt ${DT} -ts_max_steps 10 -ts_final_time 0.1 -verbose ${VERB} -num_species ${NS} -snes_monitor -plot_file_prefix s -Ez 0 -masses 1,2,4 -thermal_temps 0.2,0.02,0.02 -num_min_cells 0 ${ARGS}

run-regression:
	-${MPIEXEC} -n ${NP} ./landaufem.${PETSC_ARCH} -dm_refine 2 -snes_rtol 1.e-12 -snes_stol 1.e-12 -ts_type theta -ts_theta_theta 0.5 -ts_theta_endpoint -pc_type lu -ksp_type preonly -petscspace_order ${ORDER} -mass_petscspace_order ${ORDER} -petscspace_poly_tensor 1 -mass_petscspace_poly_tensor 1 -ts_dt 1.e-3 -ts_max_steps 1 -verbose ${VERB} -snes_monitor -num_species 3 -masses 1,2,4 -thermal_temps 0.1,0.1,0.1 ${ARGS}

run-plex:
	-${MPIEXEC} -n ${NP} ./landaufem.${PETSC_ARCH} -dm_refine ${REFINE} -snes_rtol 1.e-12 -snes_stol 1.e-12 -ts_type theta -ts_theta_theta 0.5 -ts_theta_endpoint -pc_type lu -ksp_type preonly -petscspace_order ${ORDER} -mass_petscspace_order ${ORDER} -petscspace_poly_tensor 1 -mass_petscspace_poly_tensor 1 -ts_dt ${DT} -ts_max_steps 1 -ts_final_time 0.1 -verbose ${VERB} -snes_monitor -num_species ${NS} -masses 1,1,1 -plot_file_prefix p -thermal_temps 0.2,0.02,0.02 ${ARGS}

NCC=$(shell echo 272/$(NP) | bc)

run-p4est:
	-echo ${NP} ${NCC}
	-${MPIEXEC} -n ${NP} -c ${NCC} ./landaufem.${PETSC_ARCH} -dm_refine 0 -dm_forest_maximum_refinement 12 -dm_forest_minimum_refinement 1 -dm_forest_initial_refinement ${REFINE} -dm_type p4est -snes_rtol 9.e-1 -snes_stol 9.e-1 -ts_type theta -ts_theta_theta 0.5 -ts_theta_endpoint -pc_type lu -ksp_type preonly -petscspace_order ${ORDER} -mass_petscspace_order ${ORDER} -petscspace_poly_tensor 1 -mass_petscspace_poly_tensor 1 -ts_dt ${DT} -ts_max_steps 1 -ts_final_time 1 -verbose ${VERB} -num_species ${NS} -snes_monitor -plot_file_prefix q -Ez 0 -use_amr false -refine_tol 5e+0 -coarsen_tol 1e-6 -masses 1,1835.5,7342 -thermal_temps 0.02,0.002,0.002 ${ARGS} 

run-p4est-ns:
	-${MPIEXEC} -n ${NP} ./landaufem.${PETSC_ARCH} -dm_refine 0 -dm_forest_maximum_refinement 12 -dm_forest_minimum_refinement 1 -dm_forest_initial_refinement ${REFINE} -dm_type p4est -snes_rtol 1.e-1 -snes_stol 1.e-1 -ts_type theta -ts_theta_theta 0.5 -ts_theta_endpoint -pc_type lu -ksp_type preonly -petscspace_order ${ORDER} -mass_petscspace_order ${ORDER} -petscspace_poly_tensor 1 -mass_petscspace_poly_tensor 1 -ts_dt ${DT} -ts_max_steps 1 -ts_final_time 1 -verbose ${VERB} -num_species ${NS} -snes_monitor -plot_file_prefix q -Ez 0 -use_amr false -refine_tol 5e+0 -coarsen_tol 1e-6 -masses 1,1,1 -thermal_temps 0.02,0.02,0.02 ${ARGS} 


run-vtune:
	-${MPIEXEC} -n ${NP} amplxe-cl -start-paused -collect memory-access -no-auto-finalize -data-limit 0 -r vtbw.${NP} numactl -m ${MEM} -- ./landaufem.${PETSC_ARCH} -dm_refine 4 -snes_rtol 9.99e-1 -snes_stol 9.99e-1 -ts_type theta -ts_theta_theta 0.5 -ts_theta_endpoint -pc_type lu -ksp_type preonly -petscspace_order ${ORDER} -mass_petscspace_order ${ORDER} -petscspace_poly_tensor 1 -mass_petscspace_poly_tensor 1 -ts_dt ${DT} -ts_max_steps 1 -ts_final_time 0.001 -verbose 2 -snes_monitor -num_species ${NS} -masses 1,2,4 -plot_file_prefix t -thermal_temps 0.2,0.02,0.02 ${ARGS}

#	-amplxe-cl -finalize -search-dir /global/u2/m/madams/petsc/arch-knl-opt64/lib/ -source-search-dir . -source-search-dir /global/homes/m/madams/landaufem.${PETSC_ARCH}/Plex -r res_dir.${SLURM_NODELIST}
# memory-access:0 
# amplxe-cl -finalize -search-dir /global/u2/m/madams/petsc/arch-cori-knl-opt64-intel/lib -search-dir /global/homes/m/madams/landaufem.${PETSC_ARCH}/Plex -source-search-dir /global/homes/m/madams/landaufem.${PETSC_ARCH}/Plex -r res_dir

run-vecad:
	-${MPIEXEC} -n 1 advixe-cl -collect survey --project-dir ./my_proj numactl -m ${MEM} ./landaufem.${PETSC_ARCH} -dm_refine ${REFINE} 

run-sde:
	-${MPIEXEC} -n ${NP} sde -knl -d -iform 1 -omix sde_${NP}_${NS}ns.out -i -- numactl -m ${MEM} ./landaufem.${PETSC_ARCH} -dm_refine 0 -dm_forest_maximum_refinement 12 -dm_forest_minimum_refinement 1 -dm_forest_initial_refinement ${REFINE} -dm_type p4est -snes_rtol 1.e-1 -snes_stol 1.e-1 -ts_type theta -ts_theta_theta 0.5 -ts_theta_endpoint -pc_type lu -ksp_type preonly -petscspace_order ${ORDER} -mass_petscspace_order ${ORDER} -petscspace_poly_tensor 1 -mass_petscspace_poly_tensor 1 -ts_dt ${DT} -ts_max_steps 1 -ts_final_time 1 -verbose 2 -num_species ${NS} -snes_monitor -plot_file_prefix q -Ez 0 -use_amr false -refine_tol 5e+0 -coarsen_tol 1e-6 -masses 1,1,1 -thermal_temps 0.02,0.02,0.02 ${ARGS}

# ~/stream-ai-example/parse-sde.sh my_mix.out*

.PHONY: print

# print print VAR=the-variable
print:
	@echo $($(VAR))
