/* Landau collision operator */
#include "landau.h"
#include <immintrin.h>
#if defined(HAVE_VTUNE) && defined(__INTEL_COMPILER)
#include <ittnotify.h>
#endif

#if defined(__INTEL_COMPILER)
#if defined(PETSC_USE_REAL_SINGLE)
#define MYINVSQRT(q) invsqrtf(q)
#define MYSQRT(q) sqrtf(q)
#else
#define MYINVSQRT(q) invsqrt(q)
#define MYSQRT(q) sqrt(q)
#endif
#else
#define MYINVSQRT(q) (1./PetscSqrtReal(q))
#define MYSQRT(q) PetscSqrtReal(q)
#endif

#if defined(PETSC_USE_REAL_SINGLE)
#define MYLOG logf
#define MYSMALL 1.e-12F
#define MYVERYSMALL 1.e-30F
#else
#define MYLOG log
#define MYSMALL 1.e-12
#define MYVERYSMALL 1.e-300
#endif

#if defined(PETSC_USE_REAL_SINGLE)
static PetscReal P2[] = {
  1.53552577301013293365E-4F,
  2.50888492163602060990E-3F,
  8.68786816565889628429E-3F,
  1.07350949056076193403E-2F,
  7.77395492516787092951E-3F,
  7.58395289413514708519E-3F,
  1.15688436810574127319E-2F,
  2.18317996015557253103E-2F,
  5.68051945617860553470E-2F,
  4.43147180560990850618E-1F,
  1.00000000000000000299E0F
};
static PetscReal Q2[] = {
  3.27954898576485872656E-5F,
  1.00962792679356715133E-3F,
  6.50609489976927491433E-3F,
  1.68862163993311317300E-2F,
  2.61769742454493659583E-2F,
  3.34833904888224918614E-2F,
  4.27180926518931511717E-2F,
  5.85936634471101055642E-2F,
  9.37499997197644278445E-2F,
  2.49999999999888314361E-1F
};
#else
static PetscReal P2[] = {
  1.53552577301013293365E-4,
  2.50888492163602060990E-3,
  8.68786816565889628429E-3,
  1.07350949056076193403E-2,
  7.77395492516787092951E-3,
  7.58395289413514708519E-3,
  1.15688436810574127319E-2,
  2.18317996015557253103E-2,
  5.68051945617860553470E-2,
  4.43147180560990850618E-1,
  1.00000000000000000299E0
};
static PetscReal Q2[] = {
  3.27954898576485872656E-5,
  1.00962792679356715133E-3,
  6.50609489976927491433E-3,
  1.68862163993311317300E-2,
  2.61769742454493659583E-2,
  3.34833904888224918614E-2,
  4.27180926518931511717E-2,
  5.85936634471101055642E-2,
  9.37499997197644278445E-2,
  2.49999999999888314361E-1
};
#endif
#if defined(PETSC_USE_REAL_SINGLE)
static PetscReal P1[] =
{
 1.37982864606273237150E-4F,
 2.28025724005875567385E-3F,
 7.97404013220415179367E-3F,
 9.85821379021226008714E-3F,
 6.87489687449949877925E-3F,
 6.18901033637687613229E-3F,
 8.79078273952743772254E-3F,
 1.49380448916805252718E-2F,
 3.08851465246711995998E-2F,
 9.65735902811690126535E-2F,
 1.38629436111989062502E0F
};
static PetscReal Q1[] =
{
 2.94078955048598507511E-5F,
 9.14184723865917226571E-4F,
 5.94058303753167793257E-3F,
 1.54850516649762399335E-2F,
 2.39089602715924892727E-2F,
 3.01204715227604046988E-2F,
 3.73774314173823228969E-2F,
 4.88280347570998239232E-2F,
 7.03124996963957469739E-2F,
 1.24999999999870820058E-1F,
 4.99999999999999999821E-1F
};
#else
static PetscReal P1[] =
{
 1.37982864606273237150E-4,
 2.28025724005875567385E-3,
 7.97404013220415179367E-3,
 9.85821379021226008714E-3,
 6.87489687449949877925E-3,
 6.18901033637687613229E-3,
 8.79078273952743772254E-3,
 1.49380448916805252718E-2,
 3.08851465246711995998E-2,
 9.65735902811690126535E-2,
 1.38629436111989062502E0
};
static PetscReal Q1[] =
{
 2.94078955048598507511E-5,
 9.14184723865917226571E-4,
 5.94058303753167793257E-3,
 1.54850516649762399335E-2,
 2.39089602715924892727E-2,
 3.01204715227604046988E-2,
 3.73774314173823228969E-2,
 4.88280347570998239232E-2,
 7.03124996963957469739E-2,
 1.24999999999870820058E-1,
 4.99999999999999999821E-1
};
#endif
/* elliptic functions 
 */
PETSC_STATIC_INLINE PetscReal polevl_10( PetscReal x, PetscReal coef[] )
{
  PetscReal ans;
  int       i;
  ans = coef[0];
  for (i=1; i<11; i++) ans = ans * x + coef[i];  
  return( ans );
}
PETSC_STATIC_INLINE PetscReal polevl_9( PetscReal x, PetscReal coef[] )
{
  PetscReal ans;
  int       i;
  ans = coef[0];
  for (i=1; i<10; i++) ans = ans * x + coef[i]; 
  return( ans );
}
/*
 *	Complete elliptic integral of the second kind
 */
#undef __FUNCT__
#define __FUNCT__ "ellipticE"
PETSC_STATIC_INLINE void ellipticE(PetscReal x,PetscReal *ret)
{
  x = 1 - x; /* where m = 1 - m1 */
  *ret = polevl_10(x,P2) - MYLOG(x) * (x * polevl_9(x,Q2));
}
/*
 *	Complete elliptic integral of the first kind
 */
#undef __FUNCT__
#define __FUNCT__ "ellipticK"
PETSC_STATIC_INLINE void ellipticK(PetscReal x,PetscReal *ret)
{
  x = 1 - x; /* where m = 1 - m1 */
  *ret = polevl_10(x,P1) - MYLOG(x) * polevl_10(x,Q1);
}

/* integration point functions */
/* Evaluates the tensor U=(I-(x-y)(x-y)/(x-y)^2)/|x-y| at point x,y */
/* if x==y we will return zero. This is not the correct result */
/* since the tensor diverges for x==y but when integrated */
/* the divergent part is antisymmetric and vanishes. This is not  */
/* trivial, but can be proven. */

#undef __FUNCT__
#define __FUNCT__ "LandauTensor2D"
PETSC_STATIC_INLINE void LandauTensor2D(const PetscReal x[], const PetscReal rp, const PetscReal zp, PetscReal Ud[][2], PetscReal Uk[][2])
{
  PetscReal l,s,r=x[0],z=x[1],i1func,i2func,i3func,ks,es,pi4pow,sqrt_1s,r2,rp2,r2prp2,zmzp,zmzp2,tt;
  PetscReal mask /* = !!(r!=rp || z!=zp) */;
  /* !!(zmzp2 > 1.e-12 || (r-rp) >  1.e-12 || (r-rp) < -1.e-12); */
  r2=PetscSqr(r);
  zmzp=z-zp;
  rp2=PetscSqr(rp);
  zmzp2=PetscSqr(zmzp);
  r2prp2=r2+rp2;
  l = r2 + rp2 + zmzp2;
  if      ( zmzp2 >  MYSMALL) mask = 1;
  else if ( (tt=(r-rp)) >  MYSMALL) mask = 1;
  else if (  tt         < -MYSMALL) mask = 1;
  else mask = 0;
  s = mask*2*r*rp/l; /* mask for vectorization */
  tt = 1./(1+s);
  pi4pow = 4*M_PI*MYINVSQRT(PetscSqr(l)*l);
  sqrt_1s = MYSQRT(1.+s);
   /* sp.ellipe(2.*s/(1.+s)) */
  ellipticE(2*s*tt,&es); /* 44 flops * 2 + 75 = 163 flops including 2 logs, 1 sqrt, 1 pow, 21 mult */
  /* sp.ellipk(2.*s/(1.+s)) */
  ellipticK(2*s*tt,&ks); /* 44 flops + 75 in rest, 21 mult */
  /* mask is needed here just for single precision */
  i2func = 2./((1-s)*sqrt_1s) * es;
  i1func = 4./(PetscSqr(s)*sqrt_1s + MYVERYSMALL) * mask * ( ks - (1.+s) * es);
  i3func = 2./((1-s)*(s)*sqrt_1s + MYVERYSMALL) * (es - (1-s) * ks);
  Ud[0][0]=                    pi4pow*(rp2*i1func+PetscSqr(zmzp)*i2func);
  Ud[0][1]=Ud[1][0]=Uk[0][1]= -pi4pow*(zmzp)*(r*i2func-rp*i3func);
  Uk[1][1]=Ud[1][1]=           pi4pow*((r2prp2)*i2func-2*r*rp*i3func)*mask;
  Uk[0][0]=                    pi4pow*(zmzp2*i3func+r*rp*i1func);
  Uk[1][0]=                   -pi4pow*(zmzp)*(r*i3func-rp*i2func); /* 48 mults + 21 + 21 = 90 mults and divs */
}

/* vector intrinsic elliptic functions 
 */
PETSC_STATIC_INLINE _MAVX_ polevl_10_AVX( _MAVX_ x, PetscReal coef[])
{
  _MAVX_ ans;
  int       i;
  ans = M_SET1(coef[0]);
  for (i=1; i<11; i++) ans = M_FMA(ans,x,M_SET1(coef[i])); // ans * x + coef[i];
  return( ans );
}

PETSC_STATIC_INLINE _MAVX_ polevl_9_AVX( _MAVX_ x, PetscReal coef[] )
{
  _MAVX_ ans;
  int       i;
  ans = M_SET1(coef[0]);
  for (i=1; i<10; i++) ans = M_FMA(ans,x,M_SET1(coef[i]));
  return( ans );
}
/*
 *	Complete elliptic integral of the second kind
 */
#undef __FUNCT__
#define __FUNCT__ "ellipticE_AVX"
PETSC_STATIC_INLINE void ellipticE_AVX(_MAVX_ ax, _MAVX_ one, _MAVX_ *ret)
{
  /* x = 1 - x; */ /* where m = 1 - m1 */
  _MAVX_ x2 = M_SUB(one,ax);
  *ret = M_SUB(polevl_10_AVX(x2,P2),M_MULT(M_LOG(x2),M_MULT(x2,polevl_9_AVX(x2,Q2))));
}
/*
 *	Complete elliptic integral of the first kind
 */
#undef __FUNCT__
#define __FUNCT__ "ellipticK_AVX"
PETSC_STATIC_INLINE void ellipticK_AVX(_MAVX_ ax, _MAVX_ one, _MAVX_ *ret)
{
  /* x = 1 - x; */ /* where m = 1 - m1 */
  _MAVX_ x2 = M_SUB(one,ax);
  *ret = M_SUB(polevl_10_AVX(x2,P1),M_MULT(M_LOG(x2),polevl_10_AVX(x2,Q1)));
}

static _MAVX_ s_vsmall,s_two,s_one,s_pi4,s_four;
void init_static()
{
  s_two = M_SET1(2.0);
  s_one = M_SET1(1.0);
  s_pi4 = M_SET1(4*M_PI);
  s_vsmall = M_SET1(MYVERYSMALL);
  s_four = M_SET1(4.);
}

#undef __FUNCT__
#define __FUNCT__ "LandauTensor2D_AVX"
PETSC_STATIC_INLINE void LandauTensor2D_AVX(const _MAVX_ r, const _MAVX_ z, const _MAVX_ rp, const _MAVX_ zp, _MAVX_ Ud[2][2], _MAVX_ Uk[2][2])
{
  _MAVX_ l,s,sp1,i1func,i2func,i3func,ks,es,pi4pow,sqrt_1s,r2,rp2,r2prp2,zmzp,zmzp2,mask,diff,tt;
  /* r2=PetscSqr(r); */
  r2 = M_MULT(r,r);
  /*   zmzp=z-zp; */
  zmzp = M_SUB(z,zp);
  /*   rp2=PetscSqr(rp); */
  rp2 = M_MULT(rp,rp);
  /*   zmzp2=PetscSqr(zmzp); */
  zmzp2 = M_MULT(zmzp,zmzp);
  /*   r2prp2=r2+rp2; */
  r2prp2 = M_ADD(r2, rp2);
  /*   l = r2 + rp2 + zmzp2; */
  l = M_ADD(r2, rp2);
  l = M_ADD(l, zmzp2);
  /* !!(zmzp2 > 1.e-12 || (r-rp) >  1.e-12 || (r-rp) < -1.e-12); */
  /* mask, could do EQ */
#if defined ( __AVX512F__ )
#if 0
  __declspec(align(64)) PetscReal am[LAND_VL],ar[LAND_VL],arp[LAND_VL],azmzp2[LAND_VL];
  int  ii;
  M_STORE(ar, r);
  M_STORE(arp, rp);
  M_STORE(azmzp2, zmzp2);
#pragma omp simd linear(ii) aligned(am,ar,arp,azmzp2:PETSC_MEMALIGN)
  for (ii = 0; ii < LAND_VL; ii++) {
    am[ii] = !!(ar[ii] != arp[ii] || zmzp2[ii] > 0);
  }
  mask = M_LOAD(am);
#else
#if defined(PETSC_USE_REAL_SINGLE)
  __declspec(align(64)) float one[LAND_VL] = {1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,1.};
  __mmask16 m1 = _mm512_cmp_ps_mask(r, rp, _CMP_NEQ_UQ);
  __mmask16 m2 = _mm512_cmp_ps_mask(z, zp, _CMP_NEQ_UQ);
  __mmask16 m3 = _mm512_kor(m1, m2);
  mask        = _mm512_maskz_load_ps(m3, one);
#else
  __declspec(align(64)) double one[LAND_VL] = {1.,1.,1.,1.,1.,1.,1.,1.};
  __mmask8 m1 = _mm512_cmp_pd_mask(r, rp, _CMP_NEQ_UQ);
  __mmask8 m2 = _mm512_cmp_pd_mask(z, zp, _CMP_NEQ_UQ);
  __mmask8 m3 = _mm512_kor(m1, m2);
  mask        = _mm512_maskz_load_pd(m3, one);
#endif
#endif
#else
  mask = M_CMPNE(r,rp);
  mask = M_OR(M_CMPNE(z,zp),mask);
  mask = M_AND(M_SET1(1.),mask); // register with 1 if neg, 0 otherwise, make M_CMPGT do this
#endif
  /* s = mask*2*r*rp/l; /\* mask for vectorization *\/ */
  s = M_MULT(r,rp);
  s = M_MULT(s,s_two);
  s = M_DIV(s,l);
  s = M_MULT(mask,s);
  /* sp1 = 1+s */
  sp1 = M_ADD(s, s_one);
  /* diff = l^3 */
  diff = M_MULT(l,l);
  diff = M_MULT(diff,l); 
  /*   sqrt_1s = MYSQRT(1.+s); */
  sqrt_1s = M_SQRT(sp1);
  tt = M_RECIP(sp1); /*   tt = 1./(1+s); */
  /*   pi4pow = 4*M_PI*MYINVSQRT(PetscSqr(l)*l); */
  pi4pow = M_ISQRT(diff);
  tt = M_MULT(tt,s_two);
  pi4pow = M_MULT(pi4pow,s_pi4);
  /* #pragma forceinline recursive */
  tt = M_MULT(tt,s);
  /*   ellipticE(2*s/(1+s),&es); /\* 44 flops * 2 + 75 = 163 flops including 2 logs, 1 sqrt, 1 pow, 21 mult *\/ */
#pragma forceinline recursive
  ellipticE_AVX(tt,s_one,&es);
  /*   ellipticK(2*s/(1+s),&ks); /\* 44 flops + 75 in rest, 21 mult *\/ */
#pragma forceinline recursive
  ellipticK_AVX(tt,s_one,&ks);
  /*   /\* mask is needed here just for single precision *\/ */
  /*   i2func = 2./((1-s)*sqrt_1s) * es; */
  tt = M_SUB(s_one,s); /* (1-s) */
  tt = M_MULT(tt,sqrt_1s); /* (1-s)*sqrt_1s */
  tt = M_DIV(s_two,tt); /* 2./((1-s)*sqrt_1s) */
  i2func = M_MULT(tt,es);
  /*   i1func = 4./(PetscSqr(s)*sqrt_1s + MYVERYSMALL) * mask * (ks - (1.+s) * es); */
  tt = M_MULT(s,s);
  tt = M_MULT(tt,sqrt_1s);
  tt = M_ADD(tt,s_vsmall);
  tt = M_DIV(s_four,tt);
  diff = M_MULT(sp1,es);
  tt = M_MULT(tt,mask);
  diff = M_SUB(ks,diff);
  i1func = M_MULT(tt,diff);
  /*   i3func = 2./((1-s)*(s)*sqrt_1s + MYVERYSMALL) * (es - (1-s)* ks); */  
  tt = M_SUB(s_one,s); /* (1-s) */
  diff = M_MULT(tt,ks); /* (1-s)* ks */
  tt = M_MULT(s,tt); /* (1-s)*(s) */
  tt = M_MULT(sqrt_1s,tt);
  diff = M_SUB(es,diff);
  tt = M_ADD(tt,s_vsmall);
  tt = M_DIV(s_two,tt);
  i3func = M_MULT(tt,diff);
  /*   Ud[0][0]=                    pi4pow*(rp2*i1func+PetscSqr(zmzp)*i2func); */
  tt = M_MULT(zmzp,zmzp);
  diff = M_MULT(rp2,i1func);  
  tt = M_MULT(tt,i2func);
  tt = M_ADD(tt,diff);
  Ud[0][0] = M_MULT(tt,pi4pow);
  /*   Ud[0][1]=Ud[1][0]=Uk[0][1]= -pi4pow*(zmzp)*(r*i2func-rp*i3func); */
  tt = M_MULT(i3func,rp);
  diff = M_MULT(r,i2func);
  tt = M_SUB(tt,diff);
  tt = M_MULT(zmzp,tt);
  Ud[0][1]=Ud[1][0]=Uk[0][1]= M_MULT(pi4pow,tt);
  /*   Uk[1][1]=Ud[1][1]=           pi4pow*((r2prp2)*i2func-2*r*rp*i3func)*mask; */
  tt = M_MULT(i3func,rp);
  tt = M_MULT(tt,r);
  diff = M_MULT(i2func,r2prp2);
  tt = M_MULT(tt,s_two);
  tt = M_SUB(diff,tt);
  tt = M_MULT(mask,tt);
  Uk[1][1]=Ud[1][1] = M_MULT(pi4pow,tt);
  /*   Uk[0][0]=                    pi4pow*(zmzp2*i3func+r*rp*i1func); */
  tt = M_MULT(i1func,rp);
  tt = M_MULT(tt,r);
  diff = M_MULT(i3func,zmzp2);
  tt = M_ADD(tt,diff);
  Uk[0][0]=M_MULT(pi4pow,tt);
  /*   Uk[1][0]=                   -pi4pow*(zmzp)*(r*i3func-rp*i2func);/\* 48 mults + 21 + 21 = 90 mults and divs *\/ */
  tt = M_MULT(i2func,rp);
  diff = M_MULT(i3func,r);
  tt = M_SUB(tt,diff);
  tt = M_MULT(tt,zmzp);
  Uk[1][0]=M_MULT(pi4pow,tt);
}
#undef __FUNCT__
#define __FUNCT__ "LandPointDataView"
PetscErrorCode LandPointDataView(MPI_Comm comm, LandPointData *ld)
{
  PetscErrorCode i,d,s;
  PetscFunctionBeginUser;
  for (i=0;i<ld->nip;i++) {
    PetscPrintf(comm,"ID data %4D) x = [",i);
    PetscPrintf(comm,"%10.3e ",ld->xr[i]);
    PetscPrintf(comm,"%10.3e ",ld->xz[i]);
    PetscPrintf(comm,"], f(x) = [");
    for (s=0;s<ld->ns;s++) {
      PetscPrintf(comm,"%10.3e ",ld->f[s][i]);
    }
    PetscPrintf(comm,"], grad f(x) = [");
    for (s=0;s<ld->ns;s++) {
      for (d=0;d<2;d++) PetscPrintf(comm,"%10.3e",ld->df[d][s][i]);
      if (s<ld->ns-1) PetscPrintf(comm,", ");
    }
    PetscPrintf(comm,"]\n");
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "LandPointDataCreate"
PetscErrorCode LandPointDataCreate(LandPointData *ld, PetscInt dim, PetscInt nip, PetscInt Nf)
{
  PetscErrorCode ierr, sz = nip + !!(nip%LAND_VL),jj,d;
  PetscFunctionBeginUser;
  ld->nip = nip;
  ld->ns = Nf;
  ierr = PetscMalloc2(sz,&ld->xr,sz,&ld->xz);CHKERRQ(ierr);
  for (jj=0;jj<Nf;jj++){
    ierr = PetscMalloc3(sz,&ld->f[jj],sz,&ld->df[0][jj],sz,&ld->df[1][jj]);CHKERRQ(ierr);
  }
  for (/* void */; nip < sz ; nip++) { /* zero end, should be benign */
    ld->xr[nip] = 0; 
    ld->xz[nip] = 0;
    for (jj=0;jj<Nf;jj++) {
      ld->f[jj][nip] = 0;
      for (d=0;d<dim;d++) ld->df[d][jj][nip] = 0;
    }
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "LandPointDataDestroy"
PetscErrorCode LandPointDataDestroy(LandPointData *ld)
{
  PetscErrorCode   ierr, s;
  PetscFunctionBeginUser;
  for (s=0;s<ld->ns;s++) {
    ierr = PetscFree3(ld->f[s],ld->df[0][s],ld->df[1][s]);CHKERRQ(ierr);
  }
  ierr = PetscFree2(ld->xr,ld->xz);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/* ------------------------------------------------------------------- */
#undef __FUNCT__
#define __FUNCT__ "FormLandau"
/*
  FormLandau - Evaluates Jacobian matrix.
  
  Input Parameters:
  .  globX - input vector
  .  actx - optional user-defined context
  
  Output Parameters:
  .  JacP - Jacobian matrix
  .  Bmat - optionally different preconditioning matrix
  .  globF - optional vector for residual only calc
*/
PetscErrorCode FormLandau(Vec globX,Vec globF,Mat JacP,Mat Bmat,void *actx)
{
  LandCtx           *ctx=(LandCtx*)actx;
  PetscErrorCode    ierr;
  PetscInt          cStart, cEnd, GcStart,GcEnd,cEndInterior, elemMatSize;
  DM                plex = 0, Gplex = 0;
  Vec               locX,locF;
  PetscDS           prob;
  PetscSection      section, globalSection;
  PetscScalar       *elemVec, *elemMat;
  PetscScalar       *uu, *u_x;
  const PetscInt    dim = 2;
  PetscInt          numCells,numGCells,totDim,ei,ej,Nq,*Nbf,*Ncf,Nb,Nf,ii,jj,d,f,fieldA;
  PetscInt          foffsets[LAND_NUM_SPECIES_MAX];
  PetscQuadrature   quad;
  PetscReal         **Bf, **Df, *wiLocal, *wiGlobal, nu_m0_ma[LAND_NUM_SPECIES_MAX][LAND_NUM_SPECIES_MAX], nu_0;
  const PetscReal   *quadPoints, *quadWeights;
  LandPointData     IPDataLocal,IPDataGlobal;
  PetscReal         invMass[LAND_NUM_SPECIES_MAX],Eq_m[LAND_NUM_SPECIES_MAX];
  PetscLogDouble flops;
  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(globX,VEC_CLASSID,1);
  if (!JacP) PetscValidHeaderSpecific(globF,VEC_CLASSID,2);
  if (JacP) PetscValidHeaderSpecific(JacP,MAT_CLASSID,3);
  if (Bmat) PetscValidHeaderSpecific(Bmat,MAT_CLASSID,4);
  PetscValidPointer(actx,5);
#if defined(__INTEL_COMPILER) && (__INTEL_COMPILER >= 1300) && (LAND_VL > 1)
  init_static();
#endif
#if defined(PETSC_USE_LOG)
  ierr = PetscLogEventBegin(ctx->events[1],0,0,0,0);CHKERRQ(ierr);
#endif
  ierr = DMConvert(ctx->dm, DMPLEX, &plex);CHKERRQ(ierr);
  ierr = DMConvert(ctx->serial_dm, DMPLEX, &Gplex);CHKERRQ(ierr);
  ierr = DMGetLocalVector(plex, &locX);CHKERRQ(ierr);
  ierr = VecZeroEntries(locX);CHKERRQ(ierr); /* zero BCs so don't set */
  ierr = DMGlobalToLocalBegin(plex, globX, INSERT_VALUES, locX);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(plex, globX, INSERT_VALUES, locX);CHKERRQ(ierr);
  ierr = DMPlexGetHeightStratum(plex, 0, &cStart, &cEnd);CHKERRQ(ierr);
  ierr = DMPlexGetHybridBounds(plex, &cEndInterior, NULL, NULL, NULL);CHKERRQ(ierr);
  cEnd = cEndInterior < 0 ? cEnd : cEndInterior;
  ierr = DMGetDefaultSection(plex, &section);CHKERRQ(ierr);
  ierr = DMGetDefaultGlobalSection(plex, &globalSection);CHKERRQ(ierr);
  ierr = DMGetDS(plex, &prob);CHKERRQ(ierr);
  ierr = DMPlexGetHeightStratum(Gplex, 0, &GcStart, &GcEnd);CHKERRQ(ierr); /* does not work for forest */
  ierr = DMPlexGetHybridBounds(Gplex, &cEndInterior, NULL, NULL, NULL);CHKERRQ(ierr);
  GcEnd = cEndInterior < 0 ? GcEnd : cEndInterior;
  ierr = PetscDSGetDimensions(prob, &Nbf);CHKERRQ(ierr); Nb = Nbf[0];
  ierr = PetscSectionGetNumFields(section, &Nf);CHKERRQ(ierr); assert(Nf==ctx->num_species); assert(Nf==1 || Nbf[0] == Nbf[1]);
  ierr = PetscDSGetComponents(prob, &Ncf);CHKERRQ(ierr); assert(Ncf[0]==1 && (Nf==1 || Ncf[1]==1));
  for (ii=0;ii<Nf;ii++) {
    PetscReal lnLam = 10;
    ierr = PetscDSGetFieldOffset(prob, ii, &foffsets[ii]);CHKERRQ(ierr);
    for (jj=0;jj<Nf;jj++) {
      nu_m0_ma[ii][jj] = PetscSqr(ctx->charges[ii]/ctx->m_0)*PetscSqr(ctx->charges[jj]/ctx->epsilon0)*lnLam / (8*M_PI);
    }
    invMass[ii] = ctx->m_0/ctx->masses[ii];
    Eq_m[ii] = -ctx->Ez * ctx->charges[ii]/ctx->masses[ii]*ctx->m_0; /* normalized */
  }
  for (/* void */;ii<LAND_NUM_SPECIES_MAX;ii++) foffsets[ii] = -1;
  nu_0 = nu_m0_ma[0][0];
  for (ii=0;ii<Nf;ii++) {
    for (jj=0;jj<Nf;jj++) {
      nu_m0_ma[ii][jj] *= (ctx->m_0/ctx->masses[ii])/nu_0;
    }
  }
  ierr = PetscDSGetTotalDimension(prob, &totDim);CHKERRQ(ierr);
  numCells = cEnd - cStart;
  numGCells = GcEnd - GcStart;
  ierr = PetscFEGetQuadrature(ctx->fe[0], &quad);CHKERRQ(ierr);
  ierr = PetscQuadratureGetData(quad, NULL, &Nq, &quadPoints, &quadWeights);CHKERRQ(ierr);
  flops = (PetscLogDouble)numGCells*(PetscLogDouble)Nq*(PetscLogDouble)(5.*dim*dim*Nf*Nf + 165.);
  if (JacP) {
    ierr = MatZeroEntries(JacP);CHKERRQ(ierr);
    elemMatSize = totDim*totDim; /* use DMGetWorkArray here!! */
  } else {
    ierr = DMGetLocalVector(plex, &locF);CHKERRQ(ierr);
    ierr = VecZeroEntries(locF);CHKERRQ(ierr);
    elemMatSize = 0;
    elemMat = NULL;
    ierr = DMGetWorkArray(plex, totDim, PETSC_SCALAR, &elemVec);CHKERRQ(ierr);
  }
  ierr = PetscDSGetEvaluationArrays(prob, &uu, NULL, &u_x);CHKERRQ(ierr);
  ierr = PetscDSGetTabulation(prob, &Bf, &Df);CHKERRQ(ierr);
#if defined(PETSC_USE_LOG)
  ierr = PetscLogEventEnd(ctx->events[1],0,0,0,0);CHKERRQ(ierr);
  ierr = PetscLogEventBegin(ctx->events[2],0,0,0,0);CHKERRQ(ierr);
#endif
  /* collect f data */
  if (ctx->verbose > 3) PetscPrintf(PETSC_COMM_WORLD,"[%D/%D]%s: %D IPs, %D cells, %D local cells. %s elements, totDim=%D, Nb=%D, Nq=%D, Nf=%D, elemMatSize=%D, offs=%D %D %D, computing %s\n",
				    ctx->sub_rank,ctx->sub_index,__FUNCT__,Nq*numGCells,numGCells,numCells,ctx->simplex ? "SIMPLEX" : "TENSOR", totDim, Nb, Nq, Nf, elemMatSize, foffsets[0], foffsets[1], foffsets[2],
				    JacP ? "Jacobian" : "Residual");
  ierr = LandPointDataCreate(&IPDataLocal, dim,Nq*numCells, Nf);CHKERRQ(ierr);
  ierr = LandPointDataCreate(&IPDataGlobal,dim,Nq*numGCells,Nf);CHKERRQ(ierr);
  ierr = PetscMalloc3(elemMatSize,&elemMat,numGCells*Nq,&wiGlobal,numCells*Nq,&wiLocal);CHKERRQ(ierr);
  /* cache geometry and x, f and df/dx at IPs */
  for (ei = 0; ei < numCells; ++ei) {
    PetscReal       xi[2];
    PetscReal       v0i[2],Ji[4],invJi[4], detJi;
    PetscInt        qi;
    PetscScalar     *coef = NULL;
    ierr = DMPlexComputeCellGeometryFEM(plex, cStart+ei, NULL /* fe for non-affine */, v0i, Ji, invJi, &detJi);CHKERRQ(ierr);
    ierr = DMPlexVecGetClosure(plex, section, locX, cStart+ei, NULL, &coef);CHKERRQ(ierr);
    /* create point data for cell i for Landau tensor: x, f(x), grad f(x) */
    for (qi = 0; qi < Nq; ++qi) {
      PetscInt    idx = (ei*Nq + qi);
      PetscScalar refSpaceDer[2];
      PetscInt    dOffset = 0, fOffset = 0;
      CoordinatesRefToReal(dim, dim, v0i, Ji, &quadPoints[qi*dim], xi);
      wiLocal[idx] = detJi * quadWeights[idx%Nq] * xi[0];
      IPDataLocal.xr[idx] = xi[0]; /* coordinate */
      IPDataLocal.xz[idx] = xi[1]; /* coordinate */
      /* get u & du (EvaluateFieldJets) */
      for (f = 0; f < Nf; ++f) {
	const PetscInt   Nb = Nbf[f];
	const PetscReal *Bq = &Bf[f][qi*Nb];
	const PetscReal *Dq = &Df[f][qi*Nb*dim];
	PetscInt         b, e;
	uu[fOffset] = 0.0;
	for (d = 0; d < dim; ++d) refSpaceDer[d] = 0.0;
	for (b = 0; b < Nb; ++b) {
	  const PetscInt    cidx = b;
	  uu[fOffset] += Bq[cidx]*coef[dOffset+cidx];
	  for (d = 0; d < dim; ++d) {
	    refSpaceDer[d] += Dq[cidx*dim+d]*coef[dOffset+cidx];
	  }
	}
	for (d = 0; d < dim; ++d) {
	  for (e = 0, u_x[fOffset*dim+d] = 0.0; e < dim; ++e) {
	    u_x[fOffset*dim+d] += invJi[e*dim+d]*refSpaceDer[e];
	  }
	}
	fOffset += 1;
	dOffset += Nb;
      }
      /* copy to IPDataLocal */
      for (f=0;f<Nf;f++) {
	for (d = 0; d < dim; ++d) IPDataLocal.df[d][f][idx] = u_x[f*dim+d];
	IPDataLocal.f[f][idx] = uu[f];
      }
    }
    ierr = DMPlexVecRestoreClosure(plex, section, locX, cStart+ei, NULL, &coef);CHKERRQ(ierr);
  }
  { /* all to all to get IPDataGlobal and wi */
    VecScatter  scat;
    Vec         v,w;
    PetscScalar *a;
    /* r */
    ierr = VecCreateMPIWithArray(ctx->comm,1,numCells*Nq,numGCells*Nq,IPDataLocal.xr,&v);CHKERRQ(ierr);
    ierr = VecScatterCreateToAll(v,&scat,&w);CHKERRQ(ierr);
    ierr = VecScatterBegin(scat,v,w,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
    ierr = VecScatterEnd(scat,v,w,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
    ierr = VecDestroy(&v);CHKERRQ(ierr);
    ierr = VecScatterDestroy(&scat);CHKERRQ(ierr);
    ierr = VecGetArray(w,&a);CHKERRQ(ierr);
    ierr = PetscMemcpy(IPDataGlobal.xr,a,numGCells*Nq*sizeof(PetscReal));CHKERRQ(ierr);
    ierr = VecRestoreArray(w,&a);CHKERRQ(ierr);
    ierr = VecDestroy(&w);CHKERRQ(ierr);
    /* z */
    ierr = VecCreateMPIWithArray(ctx->comm,1,numCells*Nq,numGCells*Nq,IPDataLocal.xz,&v);CHKERRQ(ierr);
    ierr = VecScatterCreateToAll(v,&scat,&w);CHKERRQ(ierr);
    ierr = VecScatterBegin(scat,v,w,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
    ierr = VecScatterEnd(scat,v,w,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
    ierr = VecDestroy(&v);CHKERRQ(ierr);
    ierr = VecScatterDestroy(&scat);CHKERRQ(ierr);
    ierr = VecGetArray(w,&a);CHKERRQ(ierr);
    ierr = PetscMemcpy(IPDataGlobal.xz,a,numGCells*Nq*sizeof(PetscReal));CHKERRQ(ierr);
    ierr = VecRestoreArray(w,&a);CHKERRQ(ierr);
    ierr = VecDestroy(&w);CHKERRQ(ierr);
    for (ii=0;ii<Nf;ii++) {
      /* f */
      ierr = VecCreateMPIWithArray(ctx->comm,1,numCells*Nq,numGCells*Nq,IPDataLocal.f[ii],&v);CHKERRQ(ierr);
      ierr = VecScatterCreateToAll(v,&scat,&w);CHKERRQ(ierr);
      ierr = VecScatterBegin(scat,v,w,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
      ierr = VecScatterEnd(scat,v,w,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
      ierr = VecDestroy(&v);CHKERRQ(ierr);
      ierr = VecScatterDestroy(&scat);CHKERRQ(ierr);
      /* VecView(w,PETSC_VIEWER_STDOUT_WORLD); */
      ierr = VecGetArray(w,&a);CHKERRQ(ierr);
      ierr = PetscMemcpy(IPDataGlobal.f[ii],a,numGCells*Nq*sizeof(PetscReal));CHKERRQ(ierr);
      ierr = VecRestoreArray(w,&a);CHKERRQ(ierr);
      ierr = VecDestroy(&w);CHKERRQ(ierr);
      /* df */
      for (d = 0; d < dim; ++d) {
	ierr = VecCreateMPIWithArray(ctx->comm,1,numCells*Nq,numGCells*Nq,IPDataLocal.df[d][ii],&v);CHKERRQ(ierr);
	ierr = VecScatterCreateToAll(v,&scat,&w);CHKERRQ(ierr);
	ierr = VecScatterBegin(scat,v,w,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
	ierr = VecScatterEnd(scat,v,w,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
	ierr = VecDestroy(&v);CHKERRQ(ierr);
	ierr = VecScatterDestroy(&scat);CHKERRQ(ierr);
	ierr = VecGetArray(w,&a);CHKERRQ(ierr);
	ierr = PetscMemcpy(IPDataGlobal.df[d][ii],a,numGCells*Nq*sizeof(PetscReal));CHKERRQ(ierr);
	ierr = VecRestoreArray(w,&a);CHKERRQ(ierr);
	ierr = VecDestroy(&w);CHKERRQ(ierr);
      }
    }
    /* wi */
    ierr = VecCreateMPIWithArray(ctx->comm,1,numCells*Nq,numGCells*Nq,wiLocal,&v);CHKERRQ(ierr);
    ierr = VecScatterCreateToAll(v,&scat,&w);CHKERRQ(ierr);
    ierr = VecScatterBegin(scat,v,w,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
    ierr = VecScatterEnd(scat,v,w,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
    ierr = VecDestroy(&v);CHKERRQ(ierr);
    ierr = VecScatterDestroy(&scat);CHKERRQ(ierr);
    ierr = VecGetArray(w,&a);CHKERRQ(ierr);
    ierr = PetscMemcpy(wiGlobal,a,numGCells*Nq*sizeof(PetscScalar));CHKERRQ(ierr);
    ierr = VecRestoreArray(w,&a);CHKERRQ(ierr);
    ierr = VecDestroy(&w);CHKERRQ(ierr);
  }
#if defined(PETSC_USE_LOG)
  ierr = PetscLogEventEnd(ctx->events[2],0,0,0,0);CHKERRQ(ierr);
#endif
  /* outer element loop j is like a regular assembly loop */
#if defined(HAVE_VTUNE) && defined(__INTEL_COMPILER)
  __SSC_MARK(0x111); // start SDE tracing, note it uses 2 underscores
  __itt_resume(); // start VTune, again use 2 underscores
#endif
  for (ej = cStart; ej < cEnd; ++ej) {
    PetscReal        v0j[2],Jj[4],invJj[4], detJj;
    PetscInt         qj,ipidx;
    PetscScalar      *coefs = NULL;
    ierr = DMPlexComputeCellGeometryFEM(plex, ej, NULL /* fe for non-affine */, v0j, Jj, invJj, &detJj);CHKERRQ(ierr);
    if (JacP) {
      ierr = PetscMemzero(elemMat, totDim *totDim * sizeof(PetscScalar));CHKERRQ(ierr);
    } else {
      ierr = PetscMemzero(elemVec, totDim * sizeof(PetscScalar));CHKERRQ(ierr);
      ierr = DMPlexVecGetClosure(plex, section, locX, ej, NULL, &coefs);CHKERRQ(ierr);
    }
    for (qj = 0; qj < Nq; ++qj) {
#if defined(__INTEL_COMPILER) && (__INTEL_COMPILER >= 1300)
      __declspec(align(PETSC_MEMALIGN)) 
#endif
	PetscScalar   gg2[LAND_NUM_SPECIES_MAX][2],gg3[LAND_NUM_SPECIES_MAX][2][2];
      PetscScalar     g2[LAND_NUM_SPECIES_MAX][2], g3[LAND_NUM_SPECIES_MAX][2][2];
      const PetscInt  nip = numGCells*Nq; /* length of inner global interation */
      PetscInt        d2,dp,d3,fieldB;
      PetscReal       xj[2],wj;
      PetscReal       * __restrict__ xr0 = IPDataGlobal.xr;
      PetscReal       * __restrict__ xz0 = IPDataGlobal.xz;
      PetscReal       * __restrict__ f0[LAND_NUM_SPECIES_MAX];
      PetscReal       * __restrict__ df0[2][LAND_NUM_SPECIES_MAX];
#if defined(PETSC_USE_LOG)
      ierr = PetscLogEventBegin(ctx->events[3],0,0,0,0);CHKERRQ(ierr);
#endif
      for (fieldA=0;fieldA<Nf;fieldA++) {
	f0[fieldA] = IPDataGlobal.f[fieldA];
#pragma omp simd
	for (d = 0; d < 2; ++d) df0[d][fieldA] = IPDataGlobal.df[d][fieldA];
      }
      /* get x_j */
      CoordinatesRefToReal(dim, dim, v0j, Jj, &quadPoints[qj*dim], xj);
      wj = detJj*quadWeights[qj] * xj[0];
      if (!JacP) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_ARG_SIZ,"Only Jacobians");
      for (d=0;d<2;d++) {
	for (f=0;f<Nf;f++) {
	  gg2[f][d] = 0;
	  for (d2=0;d2<2;d2++) gg3[f][d][d2] = 0;
	}
      }
#if defined(__INTEL_COMPILER) && (__INTEL_COMPILER >= 1300) && (LAND_VL > 1)
      PetscInt ipidx2;
      _MAVX_ xjrv = M_SET1(xj[0]); /* r */
      _MAVX_ xjzv = M_SET1( xj[1]); /* z */
      /* zero vector version of gg2 and gg3 */
      _MAVX_ gg2v[LAND_NUM_SPECIES_MAX][2],gg3v[LAND_NUM_SPECIES_MAX][2][2];
      for (fieldA=0;fieldA<Nf;fieldA++) {
	for (d = 0; d < 2; ++d) {
	  gg2v[fieldA][d] = M_ZERO ();
	  for (d2 = 0; d2 < 2; ++d2) {
	    gg3v[fieldA][d][d2] = M_ZERO();
	  }
	}
      }
#if defined(PETSC_USE_LOG)
      ierr = PetscLogEventEnd(ctx->events[3],0,0,0,0);CHKERRQ(ierr);
      ierr = PetscLogEventBegin(ctx->events[4],0,0,0,0);CHKERRQ(ierr);
      ierr = PetscLogFlops(flops);CHKERRQ(ierr);  
#endif
      for (ipidx = 0; ipidx < nip; ipidx += LAND_VL) {
	const _MAVX_ xirv = M_LOAD( &xr0[ipidx]);
	const _MAVX_ xizv = M_LOAD( &xz0[ipidx]);
	const _MAVX_ wiv  = M_LOAD( &wiGlobal[ipidx]);
	_MAVX_       fiv[LAND_NUM_SPECIES_MAX],dfiv[2][LAND_NUM_SPECIES_MAX],Udv[2][2],Ukv[2][2];
	/* get Landau tensor */
#pragma forceinline recursive
	LandauTensor2D_AVX(xjrv,xjzv,xirv,xizv,Udv,Ukv);
	/* get f, df and for K & D */
	for (fieldA=0;fieldA<Nf;fieldA++) {
	  fiv[fieldA] = M_LOAD( &f0[fieldA][ipidx] );
	  for (d = 0; d < dim; ++d) {
	    dfiv[d][fieldA] = M_LOAD( &df0[d][fieldA][ipidx] );
	  }
	}
	for (fieldA = 0; fieldA < Nf; ++fieldA) {
	  for (fieldB = 0; fieldB < Nf; ++fieldB) {
	    _MAVX_ nu2_imassB_wiv = M_SET1( nu_m0_ma[fieldA][fieldB] * invMass[fieldB]);
	    _MAVX_ nu3_imassA_f_wiv = M_SET1(-nu_m0_ma[fieldA][fieldB] * invMass[fieldA]);
	    nu2_imassB_wiv   = M_MULT(nu2_imassB_wiv,wiv);
	    nu3_imassA_f_wiv = M_MULT(nu3_imassA_f_wiv,wiv);
	    nu3_imassA_f_wiv = M_MULT(nu3_imassA_f_wiv,fiv[fieldB]);
	    for (d2 = 0; d2 < 2; ++d2) {
	      for (d3 = 0; d3 < 2; ++d3) {
		_MAVX_ tmpv1 = M_MULT(nu3_imassA_f_wiv,Udv[d2][d3]);
		_MAVX_ tmpv2 = M_MULT(Ukv[d2][d3],dfiv[d3][fieldB]);
		/* D = -U * (I \kron (fx)): g3=f: i,j,A */
		gg3v[fieldA][d2][d3] = M_ADD(tmpv1, gg3v[fieldA][d2][d3]);
		//gg3v[fieldA][d2][d3] = M_FMA(nu3_imassA_f_wiv,Udv[d2][d3],gg3v[fieldA][d2][d3]);
		/* K = U * grad(f): g2=e: i,A */
		gg2v[fieldA][d2] = M_ADD(M_MULT(nu2_imassB_wiv,tmpv2), gg2v[fieldA][d2]);
		//gg2v[fieldA][d2] = M_FMA(nu2_imassB_wiv,tmpv2,gg2v[fieldA][d2]);
	      }
	    }
	  }
	}
      } /* IPs */
      /* sum vector gg2v and gg3v into gg2 and gg3 */
      for (fieldA=0;fieldA<Nf;fieldA++) {
#if defined(__INTEL_COMPILER) && (__INTEL_COMPILER >= 1300)
	__declspec(align(PETSC_MEMALIGN))
#endif
	  PetscScalar a[LAND_VL];
	for (d = 0; d < 2; ++d) {
	  M_STORE(a, gg2v[fieldA][d]);
	  for (ii = 0; ii < LAND_VL; ii++) gg2[fieldA][d] += a[ii];
	  for (d2 = 0; d2 < 2; ++d2) {
	    M_STORE(a, gg3v[fieldA][d][d2]); // gg3v[fieldA][d][d2]
	    for (ii = 0; ii < LAND_VL; ii++) gg3[fieldA][d][d2] += a[ii];
	  }
	}
      }
#else /* !Intel */
      for (ipidx = 0; ipidx < nip; ++ipidx) {
	const PetscReal wi = wiGlobal[ipidx];
	const PetscReal *const __restrict__ xir  = &xr0[ipidx];
	const PetscReal *const __restrict__ xiz  = &xz0[ipidx];
	const PetscReal * __restrict__ fi[LAND_NUM_SPECIES_MAX];
	const PetscReal * __restrict__ dfi[2][LAND_NUM_SPECIES_MAX];
	PetscReal       Ud[2][2], Uk[2][2];
	for (fieldA=0;fieldA<Nf;fieldA++) {
	  fi[fieldA] = &f0[fieldA][ipidx];
	  for (d = 0; d < dim; ++d) {
	    dfi[d][fieldA] = &df0[d][fieldA][ipidx];
	  }
	}
#pragma forceinline recursive
	LandauTensor2D(xj,*xir,*xiz,Ud,Uk);
	for (fieldA = 0; fieldA < Nf; ++fieldA) {
	  for (fieldB = 0; fieldB < Nf; ++fieldB) {
	    for (d2 = 0; d2 < 2; ++d2) {
	      for (d3 = 0; d3 < 2; ++d3) {
		/* K = U * grad(f): g2=e: i,A */
		gg2[fieldA][d2] += nu_m0_ma[fieldA][fieldB] * invMass[fieldB] * Uk[d2][d3] * *dfi[d3][fieldB] * wi;
		//if (!fieldA && !d2) printval(nu_m0_ma[fieldA][fieldB] * invMass[fieldB] * Uk[d2][d3] * *dfi[d3][fieldB] * wi,"o",ipidx,"\n");
		/* D = -U * (I \kron (fx)): g3=f: i,j,A */
		gg3[fieldA][d2][d3] -= nu_m0_ma[fieldA][fieldB] * invMass[fieldA] * Ud[d2][d3] * *fi[fieldB] * wi;
	      }
	    }
	  }
	}
      } /* IPs */
#endif
#if defined(PETSC_USE_LOG)
      ierr = PetscLogEventEnd(ctx->events[4],0,0,0,0);CHKERRQ(ierr);
      ierr = PetscLogEventBegin(ctx->events[5],0,0,0,0);CHKERRQ(ierr);
#endif
      /* Jacobian transform */
#pragma omp simd
      for (fieldA = 0; fieldA < Nf; ++fieldA) {
        gg2[fieldA][1] -= Eq_m[fieldA]; /* add electric field term */
	for (d = 0; d < 2; ++d) {
	  g2[fieldA][d] = 0.0;
	  for (d2 = 0; d2 < 2; ++d2) 
	    g2[fieldA][d] += invJj[d*2+d2]*gg2[fieldA][d2];
	  g2[fieldA][d] *= wj;
	}
#pragma omp simd
	for (d = 0; d < 2; ++d) {
	  for (dp = 0; dp < 2; ++dp) {
	    g3[fieldA][d][dp] = 0.0;
	    for (d2 = 0; d2 < 2; ++d2) {
	      for (d3 = 0; d3 < 2; ++d3) {
		g3[fieldA][d][dp] += invJj[d*2 + d2]*gg3[fieldA][d2][d3]*invJj[dp*2 + d3];
	      }
	    }
	    g3[fieldA][d][dp] *= wj;
	  }
	}
      }
      /* assemble - on the diagonal (I,I) */
      for (fieldA = 0; fieldA < Nf; ++fieldA) {
	const PetscReal *B = Bf[fieldA], *D = Df[fieldA], *BJq = &B[qj*Nb], *DIq = &D[qj*Nb*2], *DJq = &D[qj*Nb*2];
	PetscInt        f,g;
	for (f = 0; f < Nb; ++f) {
	  const PetscInt i    = foffsets[fieldA] + f; /* Element matrix row */
	  for (g = 0; g < Nb; ++g) {
	    const PetscInt j    = foffsets[fieldA] + g; /* Element matrix column */
	    const PetscInt fOff = i*totDim + j;
	    for (d = 0; d < 2; ++d) {		  
	      elemMat[fOff] += DIq[f*2+d]*g2[fieldA][d]*BJq[g];
	      for (d2 = 0; d2 < 2; ++d2) {
		elemMat[fOff] += DIq[f*2 + d]*g3[fieldA][d][d2]*DJq[g*2 + d2];
	      }
	    }
	  }
	}
      }
#if defined(PETSC_USE_LOG)
      ierr = PetscLogEventEnd(ctx->events[5],0,0,0,0);CHKERRQ(ierr);
#endif
    } /* qj loop */
#if defined(PETSC_USE_LOG)
    ierr = PetscLogEventBegin(ctx->events[6],0,0,0,0);CHKERRQ(ierr);
#endif
    if (JacP) {
      /* debug */
      if (ctx->verbose > 4) {
	PetscPrintf(PETSC_COMM_WORLD,"E mat\n");
	for (fieldA = 0; fieldA < Nf; ++fieldA) {
	  int fieldB; PetscInt        fOff,f,g;
	  for (fieldB = 0; fieldB < Nf; ++fieldB) {
	    for (f = 0; f < Nb; ++f) {
	      for (g = 0; g < Nb; ++g) {
		const PetscInt i = fieldA*Nb + f;
		const PetscInt j = fieldB*Nb + g; /* Element matrix column */
		fOff = i*totDim + j;
		PetscPrintf(PETSC_COMM_WORLD,"%10.3e ",elemMat[fOff]);
	      }
	      PetscPrintf(PETSC_COMM_WORLD," -- last matrix offset = %D\n",fOff);
	    }
	    PetscPrintf(PETSC_COMM_WORLD,"\n");
	  }
	  PetscPrintf(PETSC_COMM_WORLD,"\n");
	}
	PetscPrintf(PETSC_COMM_WORLD,"\n");
      }
      /* assemble matrix */
      ierr = DMPlexMatSetClosure(plex, section, globalSection, JacP, ej, elemMat, ADD_VALUES);CHKERRQ(ierr);
    } else {
      ierr = DMPlexVecRestoreClosure(plex, section, locX, ej, NULL, &coefs);CHKERRQ(ierr);
      /* Add elemVec to loc */
      ierr = DMPlexVecSetClosure(plex, section, locF, ej, elemVec, ADD_ALL_VALUES);CHKERRQ(ierr);
    }
#if defined(PETSC_USE_LOG)
    ierr = PetscLogEventEnd(ctx->events[6],0,0,0,0);CHKERRQ(ierr);
#endif
  } /* ej cells loop */
#if defined(HAVE_VTUNE) && defined(__INTEL_COMPILER)
  __itt_pause(); // stop VTune
  __SSC_MARK(0x222); // stop SDE tracing
#endif
#if defined(PETSC_USE_LOG)
  ierr = PetscLogEventBegin(ctx->events[7],0,0,0,0);CHKERRQ(ierr);
#endif
  /* assemble matrix or vector */
  if (JacP) {
    ierr = MatAssemblyBegin(JacP, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(JacP, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    /* MatView(ctx->M,ctx->PETSC_VIEWER_STDOUT_WORLD); */
    ierr = MatScale(JacP, -1.0);CHKERRQ(ierr);
  } else {
    ierr = DMRestoreWorkArray(plex, totDim, PETSC_SCALAR, &elemVec);CHKERRQ(ierr);
    ierr = VecZeroEntries(globF);CHKERRQ(ierr);
    ierr = DMLocalToGlobalBegin(plex,locF,ADD_VALUES,globF);CHKERRQ(ierr);
    ierr = DMLocalToGlobalEnd(plex,locF,ADD_VALUES,globF);CHKERRQ(ierr);
    ierr = DMRestoreLocalVector(plex, &locF);CHKERRQ(ierr);
    ierr = VecScale(globF,-1.0);CHKERRQ(ierr);
  }
#if defined(PETSC_USE_LOG)
  ierr = PetscLogEventEnd(ctx->events[7],0,0,0,0);CHKERRQ(ierr);
#endif
  /* clean up */
  ierr = PetscFree3(elemMat,wiGlobal,wiLocal);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(plex, &locX);CHKERRQ(ierr);
  ierr = DMDestroy(&plex);CHKERRQ(ierr);
  ierr = DMDestroy(&Gplex);CHKERRQ(ierr);
  ierr = LandPointDataDestroy(&IPDataGlobal);CHKERRQ(ierr);
  ierr = LandPointDataDestroy(&IPDataLocal);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/* ------------------------------------------------------------------- */
#undef __FUNCT__
#define __FUNCT__ "LandIFunction"
/*
*/
PetscErrorCode LandIFunction(TS ts,PetscReal time_dummy,Vec X,Vec X_t,Vec F,void *actx)
{
  PetscErrorCode ierr;
  LandCtx        *ctx=(LandCtx*)actx;
  PetscScalar    unorm;
  PetscFunctionBeginUser;
#if defined(PETSC_USE_LOG)
  ierr = PetscLogEventBegin(ctx->events[0],0,0,0,0);CHKERRQ(ierr);
#endif
  /* add E term */
  if (!ctx->matrix_free_residual) {
    ierr = VecNorm(X,NORM_2,&unorm);CHKERRQ(ierr);
    if (ctx->normJ!=unorm) {
      ctx->normJ = unorm;
      ierr = FormLandau(X,NULL,ctx->J,ctx->J,actx);CHKERRQ(ierr);
    }
    /* mat vec for op */
    ierr = MatMult(ctx->J,X,F);CHKERRQ(ierr);CHKERRQ(ierr); /* C*f */
  } else {
    ierr = FormLandau(X,F,NULL,NULL,actx);CHKERRQ(ierr);
  }
  /* add time term */
  if (X_t) {
    ierr = MatMultAdd(ctx->M,X_t,F,F);CHKERRQ(ierr);
  }
#if defined(PETSC_USE_LOG)
  ierr = PetscLogEventEnd(ctx->events[0],0,0,0,0);CHKERRQ(ierr);
#endif
  PetscFunctionReturn(0);
}

/* --------------------------------------------------------------------- */
#undef __FUNCT__
#define __FUNCT__ "LandIJacobian"
/*
*/
PetscErrorCode LandIJacobian(TS ts,PetscReal time_dummy,Vec X,Vec U_tdummy,PetscReal shift,Mat Amat,Mat Pmat,void *actx)
{
  PetscErrorCode ierr;
  LandCtx        *ctx=(LandCtx*)actx;
  PetscScalar    unorm;
  PetscFunctionBeginUser;
  /* get collision Jacobian into A */
#if defined(PETSC_USE_LOG)
  ierr = PetscLogEventBegin(ctx->events[8],0,0,0,0);CHKERRQ(ierr);
#endif
  ierr = VecNorm(X,NORM_2,&unorm);CHKERRQ(ierr);
  if (ctx->normJ!=unorm) {
    ierr = FormLandau(X,NULL,ctx->J,ctx->J,actx);CHKERRQ(ierr);
    ctx->normJ = unorm;
  }
  /* add C */
  ierr = MatCopy(ctx->J,Pmat,SAME_NONZERO_PATTERN);CHKERRQ(ierr);
  /* add mass */
  ierr = MatAXPY(Pmat,shift,ctx->M,SAME_NONZERO_PATTERN);CHKERRQ(ierr);
  /* assemble */
  ierr = MatAssemblyBegin(Pmat,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(Pmat,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  if (Pmat != Amat) { assert(0);
    ierr = MatAssemblyBegin(Amat,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(Amat,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  }
#if defined(PETSC_USE_LOG)
  ierr = PetscLogEventEnd(ctx->events[8],0,0,0,0);CHKERRQ(ierr);
#endif
  PetscFunctionReturn(0);
}
